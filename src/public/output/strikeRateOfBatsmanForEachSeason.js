let deliveriesFile = require("../../server/deliveriesdata")
let matchesFile = require("../../server/matchesdata")


let seasonsdata = {}
for (each of matchesFile) {
    let id = each["id"]
    let season = each["season"]

    seasonsdata[id] = season
}
// console.log(seasonsdata)

let strikeratePerYear = {}

for (let eachItem of deliveriesFile) {
    let batsman = eachItem["batsman"]
    let year = seasonsdata[eachItem["match_id"]]
    let runs = eachItem["total_runs"]
    if (! strikeratePerYear.hasOwnProperty(year)) {
        strikeratePerYear[year] = {}
    }

    if (strikeratePerYear[year].hasOwnProperty(batsman))  {
        strikeRate = ((runs * 100) + strikeratePerYear[year][batsman])/2
        strikeratePerYear[year][batsman] = strikeRate
    }  
    else {
        strikeRate = runs * 100
        strikeratePerYear[year][batsman] = strikeRate

    }

}
console.log(strikeratePerYear)