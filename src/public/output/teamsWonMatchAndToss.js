let matchesfile = require("../../server/matchesdata")


teamsWonMatchAndToss = {}
for (let eachItem of matchesfile) {
        let winner = eachItem["winner"]
        let tossWinner = eachItem["toss_winner"] 
    if ( winner === tossWinner ) {
        
        if (teamsWonMatchAndToss.hasOwnProperty(winner) ) {
            teamsWonMatchAndToss[winner] += 1
        }
        else {
            teamsWonMatchAndToss[winner] = 1
        }

    }
}

console.log(teamsWonMatchAndToss)


