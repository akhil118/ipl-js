let deliveriesFile = require("../../server/deliveriesdata") 


let bowlersWithDismissals = {}
for (eachItem of deliveriesFile) {
    let bowler = eachItem["bowler"]
    let batsman = eachItem["player_dismissed"]

    if (batsman === "") {
        continue
    }
    if (! bowlersWithDismissals.hasOwnProperty(bowler)) {
        bowlersWithDismissals[bowler] = {}
    }

    if (bowlersWithDismissals[bowler].hasOwnProperty(batsman)) {
        bowlersWithDismissals[bowler][batsman] += 1
    }
    else {
        bowlersWithDismissals[bowler][batsman] = 1
    }

}

let bowlersWithtopDismissalsDict = {}
let bowlersWithtopDismissals ={}
for (const [key, value] of Object.entries(bowlersWithDismissals)) {
    const result =  Object.keys(value).reduce((a, b) => value[a] > value[b] ? a : b)
    const result1 =  Object.values(value).reduce((a, b) => value[a] > value[b] ? a : b)
    let dict = {}
    dict[result] = result1
    bowlersWithtopDismissalsDict[key] = dict 
    bowlersWithtopDismissals[key] = result1 
}
// console.log(bowlersWithtopDismissals) 

const bowlerWithTopDismissalPlayer =  Object.keys(bowlersWithtopDismissals).reduce((a, b) => bowlersWithtopDismissals[a] > bowlersWithtopDismissals[b] ? a : b)
let newDict = {}
newDict[bowlerWithTopDismissalPlayer] = bowlersWithtopDismissalsDict[bowlerWithTopDismissalPlayer]    
console.log(newDict)
