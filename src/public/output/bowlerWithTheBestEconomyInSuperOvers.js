let deliveriesFile = require("../../server/deliveriesdata")  

let bowlers = {}
let count = 0
let runs = 0
for (let eachItem of deliveriesFile) {

    let superOver = parseInt(eachItem["is_super_over"])  
    let bowler = eachItem["bowler"]
    let totalruns = parseInt(eachItem["total_runs"])
    
    if (superOver !== 0) {
        if (! bowlers.hasOwnProperty(bowler)) {
            bowlers[bowler] = {}
        }
        count=count+1
        if (! bowlers[bowler].hasOwnProperty("runs")) {
            bowlers[bowler]["runs"] = totalruns
        }

        else {
            bowlers[bowler]["runs"] += totalruns
        }

        if (! bowlers[bowler].hasOwnProperty("balls")) {
            bowlers[bowler]["balls"] = 1
        }
        else {
            bowlers[bowler]["balls"] += 1
        }

    }
}
// console.log(bowlers)


let bowlersEconomyDict = {}
for (const [key, value] of Object.entries(bowlers)) {
    let economy = (value["runs"] / value["balls"])*6
    bowlersEconomyDict[key] = economy
}
// console.log(bowlersEconomyDict)

const topEconomyBowler =  Object.keys(bowlersEconomyDict).reduce((a, b) => bowlersEconomyDict[a] < bowlersEconomyDict[b] ? a : b);
console.log(topEconomyBowler)


module.exports = topEconomyBowler