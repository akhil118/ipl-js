let matchesFile = require("../../server/matchesdata")


let playerOfMatchPerSeason = {}


for (let eachItem of matchesFile) {
    let year = eachItem["season"]
    let playerOfMatch = eachItem["player_of_match"]

    if  (! playerOfMatchPerSeason.hasOwnProperty(year) ) {

            playerOfMatchPerSeason[year] = {}

    }
    // console.log(playerOfMatchPerSeason[year])

    if (playerOfMatchPerSeason[year].hasOwnProperty(playerOfMatch)) {
        playerOfMatchPerSeason[year][playerOfMatch] += 1
    }
    else {
        playerOfMatchPerSeason[year][playerOfMatch] = 1
    }


}

// console.log(playerOfMatchPerSeason)
let playersPerSeason = {}
for (const [key, value] of Object.entries(playerOfMatchPerSeason)) {
    // console.log(key,value)
    const result =  Object.keys(value).reduce((a, b) => value[a] > value[b] ? a : b);
    playersPerSeason[key] = result
}

console.log(playersPerSeason)