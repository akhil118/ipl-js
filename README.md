# IPL js

## Name
IPL Data Project I

## Description
in this project we have written code for following problems
* Finding the number of times each team won the toss and also won the match
* Finding a player who has won the highest number of Player of the Match awards for each season
* Finding the strike rate of a batsman for each season
* Finding the highest number of times one player has been dismissed by another player
* Finding the bowler with the best economy in super overs

## Project status

it is pending according to the given instruction we have compleated it upto getting the json object now we have to give bar charts for the above 